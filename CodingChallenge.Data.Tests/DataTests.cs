﻿using CodingChallenge.Data.Classes;
using CodingChallenge.Data.Classes.Translation;
using NUnit.Framework;
using System.Collections.Generic;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {
        [TestCase]
        public void TestResumenListaVacia()
        {
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>",
                GeometricForm.Print(new List<GeometricForm>(), new Spanish()));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            Assert.AreEqual("<h1>Empty list of shapes!</h1>",
                GeometricForm.Print(new List<GeometricForm>(), new English()));
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            var cuadrados = new List<GeometricForm> {new Square(5)};

            var resumen = GeometricForm.Print(cuadrados, new Spanish());

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrado | Area 25 | Perímetro 20 <br/>Total:<br/>1 Formas Perímetro 20 Area 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            var cuadrados = new List<GeometricForm>
            {
                new Square(5),
                new Square(1),
                new Square(3)
            };

            var resumen = GeometricForm.Print(cuadrados, new English());

            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>Total:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            var formas = new List<GeometricForm>
            {
                new Square(5),
                new Circle(3),
                new EquilateralTriangle(4),
                new Square(2),
                new EquilateralTriangle(9),
                new Circle(2.75m),
                new EquilateralTriangle(4.2m),
                new Trapeze(8, 6, 1),
                new Trapeze(9, 4, 6)
            };

            var resumen = GeometricForm.Print(formas, new English());

            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13,01 | Perimeter 18,06 <br/>3 Triangles | Area 49,64 | Perimeter 51,6 <br/>2 Trapezoids | Area 46 | Perimeter 41 <br/>Total:<br/>9 shapes Perimeter 138,66 Area 137,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            var formas = new List<GeometricForm>
            {
                new Square(5),
                new Circle(3),
                new EquilateralTriangle(4),
                new Square(2),
                new EquilateralTriangle(9),
                new Circle(2.75m),
                new EquilateralTriangle(4.2m),
                new Trapeze(8, 6, 1),
                new Trapeze(9, 4, 6)
            };

            var resumen = GeometricForm.Print(formas, new Spanish());

            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Cuadrados | Area 29 | Perímetro 28 <br/>2 Círculos | Area 13,01 | Perímetro 18,06 <br/>3 Triángulos | Area 49,64 | Perímetro 51,6 <br/>2 Trapecios | Area 46 | Perímetro 41 <br/>Total:<br/>9 Formas Perímetro 138,66 Area 137,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnItaliano()
        {
            var formas = new List<GeometricForm>
            {
                new Square(5),
                new Circle(3),
                new EquilateralTriangle(4),
                new Square(2),
                new EquilateralTriangle(9),
                new Circle(2.75m),
                new EquilateralTriangle(4.2m),
                new Trapeze(8, 6, 1),
                new Trapeze(9, 4, 6)
            };

            var resumen = GeometricForm.Print(formas, new Italian());

            Assert.AreEqual(
                "<h1>Rapporto del modulo</h1>2 Piazze | La zona 29 | Perimetro 28 <br/>2 Cerchi | La zona 13,01 | Perimetro 18,06 <br/>3 Triangoli | La zona 49,64 | Perimetro 51,6 <br/>2 trapezi | La zona 46 | Perimetro 41 <br/>Totale:<br/>9 Forme Perimetro 138,66 La zona 137,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConUnCuadradoEnItaliano()
        {
            var cuadrados = new List<GeometricForm> { new Square(5) };

            var resumen = GeometricForm.Print(cuadrados, new Italian());

            Assert.AreEqual("<h1>Rapporto del modulo</h1>1 Piazza | La zona 25 | Perimetro 20 <br/>Totale:<br/>1 Forme Perimetro 20 La zona 25", resumen);
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnItaliano()
        {
            Assert.AreEqual("<h1>Elenco vuoto di forme!</h1>",
                GeometricForm.Print(new List<GeometricForm>(), new Italian()));
        }

    }
}
