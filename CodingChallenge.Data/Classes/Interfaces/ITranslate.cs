﻿using CodingChallenge.Data.Classes.Models;

namespace CodingChallenge.Data.Classes.Interfaces
{
    public interface ITranslate
    {
        string GetMessage();

        string GetHeader();

        FooterModel GetFooter();

        string GetLine(int amount, decimal area, decimal perimeter, TypeForms type);

        string TranslateForm(TypeForms type, int amount);
    }
}
