﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Data.Classes.Models
{
    public class FooterModel
    {
        public string Total { get; set; }

        public string Perimeter { get; set; }

        public string Form { get; set; }

        public string Area { get; set; }
    }
}
