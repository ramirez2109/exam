﻿namespace CodingChallenge.Data.Classes.Models
{
    public enum TypeForms
    {
        Square = 1,
        Circle = 2,
        EquilateralTriangle = 3,
        Trapeze = 4
    }
}
