﻿using CodingChallenge.Data.Classes.Interfaces;
using CodingChallenge.Data.Classes.Models;

namespace CodingChallenge.Data.Classes.Translation
{
    public class Italian : ITranslate
    {
        public string GetMessage()
        {
            var message = "Elenco vuoto di forme!";

            return message;
        }

        public string GetHeader()
        {
            var message = "Rapporto del modulo";

            return message;
        }

        public FooterModel GetFooter()
        {
            var footerResult = new FooterModel();

            footerResult.Total = "Totale";
            footerResult.Area = "La zona";
            footerResult.Perimeter = "Perimetro";
            footerResult.Form = "Forme";

            return footerResult;
        }

        public string GetLine(int amount, decimal area, decimal perimeter, TypeForms type)
        {
            if (amount > 0)
            {

                return $"{amount} {TranslateForm(type, amount)} | {"La zona"} {area:#.##} | {"Perimetro"} {perimeter:#.##} <br/>";

            }

            return string.Empty;
        }

        public string TranslateForm(TypeForms type, int amount)
        {
            switch (type)
            {
                case TypeForms.Square:
                    return amount == 1 ? "Piazza" : "Piazze";
                case TypeForms.Circle:
                    return amount == 1 ? "Cerchio" : "Cerchi";
                case TypeForms.EquilateralTriangle:
                    return amount == 1 ? "Triangolo" : "Triangoli";
                case TypeForms.Trapeze:
                    return amount == 1 ? "Trapezio" : "trapezi";
            }

            return string.Empty;
        }
    }
}
