﻿using CodingChallenge.Data.Classes.Models;

namespace CodingChallenge.Data.Classes
{
    public class Trapeze : GeometricForm
    {        
        private readonly decimal _minorBase;
        private readonly decimal _majorBase;
        private readonly decimal _height;

        public Trapeze(decimal side, decimal majorBase, decimal height) : base(side, TypeForms.Trapeze)
        {            
            this._minorBase = side;
            this._majorBase = majorBase;
            this._height = height;
        }

        public override decimal CalculateArea()
        {
            return ((_minorBase + _majorBase) / 2) * _height;
        }

        public override decimal CalculatePerimeter()
        {
            return (2 * _height) + (_minorBase + _majorBase);                
        }
    }
}
