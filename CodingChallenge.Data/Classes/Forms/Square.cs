﻿using CodingChallenge.Data.Classes.Models;

namespace CodingChallenge.Data.Classes
{
    public class Square : GeometricForm
    {
        private readonly decimal _side;

        public Square(decimal side) : base(side, TypeForms.Square)
        {
            _side = side;
        }

        public override decimal CalculateArea()
        {
            return _side * _side;
        }

        public override decimal CalculatePerimeter()
        {
            return _side * 4;
        }

    }
}
