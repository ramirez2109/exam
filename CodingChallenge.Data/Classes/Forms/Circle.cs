﻿using CodingChallenge.Data.Classes.Models;
using System;

namespace CodingChallenge.Data.Classes
{
    public class Circle : GeometricForm
    {
        private readonly decimal _side;

        public Circle(decimal side) : base(side, TypeForms.Circle)
        {
            this._side = side;
        }

        public override decimal CalculateArea()
        {
            return (decimal)Math.PI * (_side / 2) * (_side / 2);
        }

        public override decimal CalculatePerimeter()
        {
            return (decimal)Math.PI * _side;
        }
    }
}
