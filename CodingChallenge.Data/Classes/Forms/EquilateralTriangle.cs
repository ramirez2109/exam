﻿using CodingChallenge.Data.Classes.Models;
using System;

namespace CodingChallenge.Data.Classes
{
    public class EquilateralTriangle : GeometricForm
    {
        private readonly decimal _side;

        public EquilateralTriangle(decimal side) : base(side, TypeForms.EquilateralTriangle)
        {
            this._side = side;
        }

        public override decimal CalculateArea()
        {
            return ((decimal)Math.Sqrt(3) / 4) * _side * _side;
        }

        public override decimal CalculatePerimeter()
        {
            return _side * 3;
        }
    }
}
